%%% @doc
%%% Barnsley's Fern GUI
%%%
%%% This process is responsible for creating the main GUI frame displayed to the user.
%%%
%%% Reference: http://erlang.org/doc/man/wx_object.html
%%% @end

-module(bf_gui).
-vsn("0.1.3").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-include_lib("wx/include/gl.hrl").
-export([show/2]).
-export([start_link/1]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").


-record(s,
        {frame     = none :: none | wx:wx_object(),
         gl        = new  :: old | new,
         slider    = none :: none | wx:wx_object(),
         button    = none :: none | wx:wx_object(),
         button_id = 0    :: integer(),
         canvas    = none :: none | wx:wx_object(),
         points    = []   :: [{number(), number()}],
         tpin      = none :: none | pin(),
         rpin      = none :: none | pin(),
         tx        = -2.0 :: float(),
         ty        = -2.0 :: float(),
         rx        =  0.0 :: float(),
         ry        =  0.0 :: float()}).


-type state() :: term().
-type pin()   :: {ClickX :: non_neg_integer(),
                  ClickY :: non_neg_integer(),
                  OrigX  :: non_neg_integer(),
                  OrigY  :: non_neg_integer()}.



%%% Interface functions

show(Points, Iterations) ->
    wx_object:cast(?MODULE, {show, Points, Iterations}).



%%% Startup Functions

start_link(Title) ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, Title, []).


init(Title) ->
    ok = log(info, "GUI starting..."),
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, Title),
    MainSz = wxBoxSizer:new(?wxVERTICAL),
    CtrlSz = wxBoxSizer:new(?wxHORIZONTAL),
    SStyle = [{style, ?wxSL_HORIZONTAL bor ?wxSL_LABELS}],
    Slider = wxSlider:new(Frame, ?wxID_ANY, 100, 1, 100000, SStyle),
    Button = wxButton:new(Frame, ?wxID_ANY, [{label, "Iterate!"}]),
    ButtonID = wxButton:getId(Button),
    CanvasAttributes =
        [{style, ?wxFULL_REPAINT_ON_RESIZE},
         {attribList,
          [?WX_GL_RGBA,
           ?WX_GL_MIN_RED,        8,
           ?WX_GL_MIN_GREEN,      8,
           ?WX_GL_MIN_BLUE,       8,
           ?WX_GL_DEPTH_SIZE,     24,
           ?WX_GL_DOUBLEBUFFER,
           ?WX_GL_SAMPLE_BUFFERS, 1,
           ?WX_GL_SAMPLES,        4,
           0]}],
    Canvas = wxGLCanvas:new(Frame, CanvasAttributes),
    ok = wxGLCanvas:connect(Canvas, paint),
    ok = wxGLCanvas:connect(Canvas, left_down),
    ok = wxGLCanvas:connect(Canvas, left_up),
    ok = wxGLCanvas:connect(Canvas, right_down),
    ok = wxGLCanvas:connect(Canvas, right_up),
    ok = wxGLCanvas:connect(Canvas, motion),
    _ = wxSizer:add(CtrlSz, Slider, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxSizer:add(CtrlSz, Button, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxSizer:add(MainSz, CtrlSz, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxSizer:add(MainSz, Canvas, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxFrame:createStatusBar(Frame),
    ok = wxFrame:setStatusText(Frame, "0"),
    _ = wxFrame:setSizer(Frame, MainSz),
    _ = wxSizer:layout(MainSz),
    ok = wxFrame:setClientSize(Frame, 800, 600),

    ok = wxFrame:connect(Frame, close_window),
    ok = wxFrame:connect(Frame, command_button_clicked),
    ok = wxFrame:center(Frame),
    true = wxFrame:show(Frame),
    GL =
        case erlang:system_info(otp_release) >= "24" of
            true ->
                true = wxGLCanvas:setCurrent(Canvas, wxGLContext:new(Canvas)),
                new;
            false ->
                ok = wxGLCanvas:setCurrent(Canvas),
                old
        end,
    ok = gl:enable(?GL_DEPTH_TEST),
    ok = gl:depthFunc(?GL_LESS),
    ok = gl:enable(?GL_CULL_FACE),
    ok = gl:enable(?GL_MULTISAMPLE),
    ok = gl:cullFace(?GL_BACK),
    State = #s{frame     = Frame,
               gl        = GL,
               slider    = Slider,
               button    = Button,
               button_id = ButtonID,
               canvas    = Canvas},
    _ = draw(State),
    {Frame, State}.

draw(#s{frame = Frame, canvas = Canvas, points = Points,
        tx = TX, ty = TY, rx = RX, ry = RY}) ->
    ok = gl:clearColor(0.1, 0.1, 0.2, 1.0),
    ok = gl:color3f(1.0, 1.0, 1.0),
    {W, H} = wxFrame:getClientSize(Frame),
    ok = gl:viewport(0, 0, W, H),
    ok = gl:matrixMode(?GL_PROJECTION),
    ok = gl:loadIdentity(),
    ok = gl:ortho(-3.0, 3.0, -3.0 * H / W, 3 * H / W, -20.0, 20.0),
    ok = gl:matrixMode(?GL_MODELVIEW),
    ok = gl:loadIdentity(),
    ok = gl:clear(?GL_COLOR_BUFFER_BIT bor ?GL_DEPTH_BUFFER_BIT),
    ok = gl:translatef(TX, TY, -3.0),
    ok = gl:rotatef(-45.0, 0.0, 0.0, 1.0),
    ok = gl:rotatef(RY, 1.0, 0.0, 0.0),
    ok = gl:rotatef(RX, 0.0, 1.0, 0.0),
    ok = gl:'begin'(?GL_LINES),
    ok = gl:'end'(),
    ok = gl:'begin'(?GL_POINTS),
    ok = gl:color3f(1.0, 1.0, 1.0),
    T = fun({X, Y}) -> gl:vertex3f(X / 2, Y / 2, 0.0) end,
    L = fun(Ps) -> lists:foreach(T, Ps) end,
    ok = lists:foreach(L, Points),
    ok = gl:'end'(),
    wxGLCanvas:swapBuffers(Canvas).


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast({show, Points, Iterations}, State) ->
    NewState = do_show(Points, Iterations, State),
    _ = draw(NewState),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_event(Event, State) -> {noreply, NewState}
    when Event    :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The wx_object:handle_event/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_event(#wx{id = ButtonID, event = #wxCommand{type = command_button_clicked}},
             State = #s{button_id = ButtonID, slider = Slider}) ->
    ok = do_iterate(Slider),
    {noreply, State};
handle_event(#wx{event = #wxPaint{}}, State) ->
    _ = draw(State),
    {noreply, State};
handle_event(#wx{event = #wxMouse{type = motion, leftDown = true, x = X, y = Y}},
             State) ->
    NewState = do_traverse(X, Y, State),
    _ = draw(NewState),
    {noreply, NewState};
handle_event(#wx{event = #wxMouse{type = motion, leftDown = false}},
             State = #s{tpin = {_, _, _, _}}) ->
    {noreply, State#s{tpin = none}};
handle_event(#wx{event = #wxMouse{type = motion, rightDown = true, x = X, y = Y}},
             State) ->
    NewState = do_rotate(X, Y, State),
    _ = draw(NewState),
    {noreply, NewState};
handle_event(#wx{event = #wxMouse{type = motion, rightDown = false}},
             State = #s{rpin = {_, _, _, _}}) ->
    {noreply, State#s{rpin = none}};
handle_event(#wx{event = #wxMouse{}}, State) ->
    {noreply, State};
handle_event(#wx{event = #wxClose{}}, State = #s{frame = Frame}) ->
    ok = bf_con:stop(),
    ok = wxWindow:destroy(Frame),
    {noreply, State};
handle_event(Event, State) ->
    ok = tell(info, "Unexpected event ~tp State: ~tp", [Event, State]),
    {noreply, State}.



code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().


%%% Doer Functions

do_show(New, Iterations, State = #s{points = Points, frame = Frame}) ->
    ok = wxFrame:setStatusText(Frame, integer_to_list(Iterations)),
    State#s{points = [New | Points]}.


do_iterate(Slider) ->
    Count = wxSlider:getValue(Slider),
    bf_con:iterate(Count).


do_traverse(X, Y, State = #s{tpin = none, tx = TX, ty = TY}) ->
    TPIN = {X, Y, TX, TY},
    State#s{tpin = TPIN};
do_traverse(X, Y, State = #s{tpin = {PX, PY, OrigX, OrigY}}) ->
    TX = OrigX - ((PX - X) / 40),
    TY = OrigY + ((PY - Y) / 40),
    State#s{tx = TX, ty = TY}.


do_rotate(X, Y, State = #s{rpin = none, rx = RX, ry = RY}) ->
    RPIN = {X, Y, RX, RY},
    State#s{rpin = RPIN};
do_rotate(X, Y, State = #s{rpin = {PX, PY, OrigX, OrigY}}) ->
    RX = OrigX + ((PX - X) / -2),
    RY = OrigY + ((PY - Y) / -2),
    State#s{rx = RX, ry = RY}.
